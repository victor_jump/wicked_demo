#include <iostream>
#include <string>
#include <list>
#include <vector>
#include <map>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_mixer.h>
#include <SDL_ttf.h>
#include <cmath>



using namespace std;

class game
{
public:

    // SECONDARY CLASSES-----------------------------------------------------------



    class Sprite
    {
    public:
        Sprite();
        ~Sprite();
        //initializing functions
        void loadSpriteFromSpriteSheet(char* iPath, game mgame, int startingX, int startingY);
        //secondary functions--------------------
        //Function that loads frame into renderer
        void loadFrame(int iFrame, game mgame, SDL_Rect targetRect);
        //Function that updates current frame
        void updateFrame();




        //----------------Set-get functions
        int getUpdateTimer()
        {
            return mUpdateTimer;
        }

        int getCurrentFrame()
        {
            return mCurrentFrame;
        }
        int getCurrentDirection()
        {
            return mSpriteDirection;
        }
        int getCurrentSpriteStatus()
        {
            return mSpriteState;
        }


        void setUpdateTimer(int iTime)
        {
            mUpdateTimer = iTime;
        }

        void setCurrentFrame(int iFrame)
        {
            mCurrentFrame = iFrame;
        }

        void setCurrentDirection(int iDirection_flag)
        {
            mSpriteDirection = iDirection_flag;
        }
        void setCurrentSpriteStatus(int  iStatus_flag)
        {
             mSpriteState  |= iStatus_flag;
        }
        void removeCurrentSpriteStatus(int iStatus_flag)
        {
            mSpriteState &= ~iStatus_flag;
        }


    private:
        SDL_Texture* mSpriteTexture;
        SDL_Rect mSpriteSheet[17];
        SDL_Rect  mTargetRect;
        SDL_Surface* mSpriteSurface;
        Uint32 mSpriteState;
        Uint32  mCurrentFrame;
        Uint32  mSpriteDirection;
        double mUpdateTimer;

    };



    //##########################GAME_EVENTS#################################

    class Event_Buffer
    {
    public:
        Event_Buffer();
        ~Event_Buffer();
        void saveEvent(SDL_Event *iEvent);
        void clearBuffer();
        SDL_Event* popEvent();
    private:
        std::list<SDL_Event*> mEventStorage;
    };



    class Text_box
       {
       public:
           Text_box();
           ~Text_box();
            void setText(char *iText, game mgame);
            void setTitle(char* iTitle, game mgame);
            void loadTextBox(game mgame);
        //get set area
        void setState(int iState)
        {
            mState |= iState;
        }
        int getState()
        {
            return mState;
        }

        void removeState(int iState)
        {
            mState &= ~iState;
        }

        void setBackground(SDL_Texture* iBackground)
        {
            mBackground = iBackground;
        }
        void setPortrait(char* iPath, game mgame)
        {
            SDL_Surface* buffer;
             buffer = IMG_Load(iPath);
             mPortrait = SDL_CreateTextureFromSurface(mgame.getRenderer(), buffer);
             delete buffer;
        }
        void setTitleTexture(SDL_Texture* iTitleTexture)
         {
              mTitleTexture = iTitleTexture;
         }
        void setMainFont(char* iFontPath)
        {
            mTextFont = TTF_OpenFont(iFontPath, 22);
        }
        void setTitleFont(char*  iFontPath)
        {
            mTitleFont = TTF_OpenFont(iFontPath, 14);
        }
        void setTextRect(SDL_Rect iRect)
        {
            mTextRect = iRect;
        }
        void setTitleRect(SDL_Rect iRect)
        {
             mTitleRect = iRect;
        }
        void setWindowRect(SDL_Rect iRect)
        {
              mWindowRect = iRect;
        }
        void setPortraitRect(SDL_Rect iRect)
        {
            mPortraitRect = iRect;
        }

        void setTextColor(SDL_Color iColor)
        {
            mTextColor = iColor;
        }
        void setTitleColor(SDL_Color iColor)
        {
            mTitleColor = iColor;
        }
    private:

        int mState;
        SDL_Texture* mPortrait;
        SDL_Texture* mBackground;
        SDL_Texture* mTextTexture;
        SDL_Texture* mTitleTexture;
        SDL_Color mTextColor;
        SDL_Color mTitleColor;
        TTF_Font* mTextFont;
        TTF_Font* mTitleFont;
        SDL_Rect mWindowRect;
        SDL_Rect mPortraitRect;
        SDL_Rect mTitleRect;
        SDL_Rect mTextRect;
    };





//########################GAME_OBJECTS#####################################
     class Game_Object
     {
     public:
         virtual void processObjects(game mgame);
          Game_Object();
          Game_Object(SDL_Rect iRect);
         ~Game_Object();
         void setRect(SDL_Rect  iRect)
         {
             mRect = iRect;
         }
         SDL_Rect getRect()
         {
             return mRect;
         }
     protected:
         SDL_Rect mRect;

     };

     class Text_rect: public Game_Object
     {
     public:
         virtual void processObjects(game& mgame);
         Text_rect();
         ~Text_rect();
         Text_rect(SDL_Rect iRect, char* iPortraitPath, char* iTitle, char* iText);
         void setTitle(char* iTitle)
         {
             mTitle = iTitle;
         }
         void setText(char* iText)
         {
             mText = iText;
         }
         void setPortraitPath(char* iPortraitPath)
         {
             mPortraitPath = iPortraitPath;
         }

     protected:
         char* mPortraitPath;
         char* mTitle;
         char* mText;
     };

     class Player: public Game_Object
     {
     public:
             Player();
             Player(char* iPath, int StartX, int StartY, game iGame, SDL_Rect iRect);
             ~Player();
             void processObjects(game mgame);
             void setState(int iState)
             {
                 mState |= iState;
             }
             int getState()
             {
                 return mState;
             }
             void removeState(int iState)
             {
                 mState &= ~iState;
             }

             void setSpriteDirection(int iDirection)
             {
                 mSprite.setCurrentDirection(iDirection);
             }
             void setSpriteStatus(int iStatus)
             {
                 mSprite.setCurrentSpriteStatus(iStatus);
             }
             void removeSpriteStatus(int iStatus)
             {
                 mSprite.removeCurrentSpriteStatus(iStatus);
             }

             Sprite getSprite()
             {
                 return mSprite;
             }
             double getMovementTimer()
             {
               return mMovementTimer;
             }
             void setMovementTimer(double iTimer)
             {
                 mMovementTimer = iTimer;
             }
             void putItem(int iItem)
             {
                 mInventory |= iItem;
             }
             void removeItem(int iItem)
             {
                 mInventory &= ~iItem;
             }
             int getInventory()
             {
                 return mInventory;
             }

     protected:
              int mState;
              int mInventory;
              Sprite mSprite;
              double mMovementTimer;
     };
     class Pickable:public Game_Object
     {
          public:
         Pickable();
         ~Pickable();
         Pickable(SDL_Rect iRect, char* iPicturePath, game &mgame, char* iTitle, char* iPortraitPath, char* iText);
         void processObjects(game& mgame);
         void removeState(int iState)
         {
             mState &= ~iState;
         }

         private:
         int mState;
         SDL_Texture* mTexture;
         Text_rect* mPickText;
     };

     class Npc: public Game_Object
     {
     public:
         void setState(int iState)
         {
             mState |= iState;
         }
         int getState()
         {
             return mState;
         }
         void removeState(int iState)
         {
             mState &= ~iState;
         }

         void setSpriteDirection(int iDirection)
         {
             mSprite.setCurrentDirection(iDirection);
         }
         void setSpriteStatus(int iStatus)
         {
             mSprite.setCurrentSpriteStatus(iStatus);
         }
         void removeSpriteStatus(int iStatus)
         {
             mSprite.removeCurrentSpriteStatus(iStatus);
         }

         Sprite getSprite()
         {
             return mSprite;
         }
         double getMovementTimer()
         {
           return mMovementTimer;
         }
         void setMovementTimer(double iTimer)
         {
             mMovementTimer = iTimer;
         }
         void setDialogueLine(int iNumber, char* iPortraitPath, char* iTitle, char* iText)
         {
              mDialogue[iNumber] = new Text_rect(mRect, iPortraitPath, iTitle, iText);

         }
         void setConditionText(char* iPortraitPath, char* iTitle, char* iText)
         {
             mConditionText = new Text_rect(mRect, iPortraitPath, iTitle, iText);
         }

         void processObjects(game &mgame);
         Npc();
         Npc(char* iPath, int StartX, int StartY, game iGame, SDL_Rect iRect);
         ~Npc();
     protected:
         int mState;
         Sprite mSprite;
         double mMovementTimer;
         double mDialogueTimer;
         int mDialogueIterator;
         std::map<int, Text_rect*> mDialogue;
         Text_rect* mConditionText;
     };



     class Picture: public Game_Object
     {
     public:
         Picture();
         ~Picture();
         Picture(char* iPath, game iGame, SDL_Rect iTargetRect);
         void processObjects(game mgame);
         void setPicture(SDL_Texture* iTexture)
         {
             mTexture = iTexture;
         }

         SDL_Texture* getTexture()
         {
             return mTexture;
         }

     protected:
         SDL_Texture* mTexture;
     };


     class Scrolling_Picture: public Picture
     {
     public:
         Scrolling_Picture();
         Scrolling_Picture(char* iPath, game iGame, SDL_Rect iTargetRect, unsigned int iDirection);
          ~Scrolling_Picture();
         void processObjects(game mgame);
         void setScrollingDirection(unsigned int iScrollingDirection)
         {
             mScrollingDirection = iScrollingDirection;
         }

     protected:
         unsigned int mScrollingDirection;
         double mTimer;
     };




    //##################################### MAIN FUNCTIONS ########################
    game();
   ~game();
    //----initializing functions
    void start();
    void loadGraphics();
    void loadGameObjects();
    void loadMap();
    //----Event processing functions
    void handleEvents();
    //---------------Function to process keyboard input
    void processKeyboardEvents();
    //----Other main functions
    void render();
    void clean();
    void update();
    //----additional functions
    bool collisionCheck(SDL_Rect Arect, SDL_Rect Brect);

    //Set section-----------------
    void setRunningStatus(bool iStatus)
    {
        isRunning = iStatus;
    }
    bool getRunningStatus()
    {
        return isRunning;
    }

    //Get section--------------------
    SDL_Event* getEvent()
    {
        return mainEvent;
    }
    SDL_Renderer* getRenderer()
    {
        return mainRenderer;
    }

     Text_box& getTextbox()
    {
        return mainTextbox;
    }
     Player*& getPlayer()
     {
         return gamePlayer;
     }

     //Event_Buffer

private:
    bool isRunning = true;

    //------main
    SDL_Window* mainWindow = NULL;
    SDL_Renderer* mainRenderer = NULL;
    SDL_Event* mainEvent = new SDL_Event();
    Event_Buffer event_buffer;
    const Uint8 *keystates;
    Text_box mainTextbox;
    //------graphics
    std::map<char*, SDL_Rect>  rectStorage;
    std::map<char*, Mix_Music*> musicStorage;
    std::map<char*, Game_Object*> objectsStorage;
    std::map<char*, Picture*> pictureStorage;
    std::map<char*, Scrolling_Picture*> scrollingPictureStorage;
    std::map<char*, Text_rect*>  textRectStorage;
    std::map<char*, Npc*> npcStorage;
    std::map<char*, Pickable*> pickableStorage;
   //-----Event generators

    Player* gamePlayer;
    //------music
    Mix_Music* bgm;
    Mix_Chunk* windowSound;
    //-----timers
    double keyTimer;
    //----crutches
};
