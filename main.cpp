#include <iostream>
#include <game.h>


using namespace std;

int main(int argc, char *argv[])
{
    game mgame;
    mgame.start();
    while(mgame.getRunningStatus())
    {
        mgame.handleEvents();
        mgame.update();
        mgame.render();
    }
    return 0;
}

