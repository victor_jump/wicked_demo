TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    game.cpp \
    main.cpp


QMAKE_CXXFLAGS += -std=c++11

INCLUDEPATH += "C:\LIBRARIES\SDL2\SDL_include"
LIBS += -LC:\LIBRARIES\SDL2 \
        -lmingw32 \
        -lSDL2main \
        -lSDL2 \
        -mwindows\
        -lSDL2_ttf \
        -lSDL2_image \
        -lSDL2_mixer \




HEADERS += \
    game.h
