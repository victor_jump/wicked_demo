#include "game.h"



using namespace std;




// ENUMERATORS AND TWEAKS #############################################
enum SPRITE_DIRECTION
{
    SPRITE_DIRECTION_UP = 13,
    SPRITE_DIRECTION_RIGHT = 9,
    SPRITE_DIRECTION_DOWN = 1,
   SPRITE_DIRECTION_LEFT = 5
};

enum SPRITE_STATE
{
    SPRITE_STATE_ANIMATING = 1,
    SPRITE_STATE_MOVING = 2

};

enum TEXTBOX_STATE
{
    TEXTBOX_STATE_SHOWN = 1
};

enum SCROLLING_PICTURE_DIRECTION
{
    SCROLLING_PICTURE_DIRECTION_UP = 1,
    SCROLLING_PICTURE_DIRECTION_LEFT= 2,
    SCROLLING_PICTURE_DIRECTION_RIGHT = 4,
    SCROLLING_PICTURE_DIRECTION_DOWN = 8
};

enum PLAYER_STATE
{
    PLAYER_STATE_USING = 1
};

enum GAME_EVENT_TYPE
{
    GAME_EVENT_TEXTBOX = 1
};

enum PICKABLE_STATE
{
    PICKABLE_LYING = 1,
    PICKABLE_PICKED = 2
};

enum ITEM_LIST
{
    ITEM_LIST_CURRY = 1
};




game::game()
    :keystates(NULL)
{

}






//###################### MECHANICS SECTION ###########################################



//MAIN FUNCTIONS
//Main Initializing function-------------------------------------------
void game::start()
{
    //Initalizing basics
    SDL_Init(SDL_INIT_EVERYTHING);
    IMG_Init(IMG_INIT_PNG);
    IMG_Init(IMG_INIT_JPG);
    TTF_Init();
    Mix_Init(MIX_INIT_MP3);
    Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 4096);

    keyTimer  = SDL_GetTicks();
    mainWindow = SDL_CreateWindow("WICKED", SDL_WINDOWPOS_CENTERED,
        SDL_WINDOWPOS_CENTERED, 1366, 768,
        SDL_WINDOW_RESIZABLE);
    if (mainWindow == NULL)
    {
        cout << "window couldn't be created";
    }

    mainRenderer = SDL_CreateRenderer(mainWindow, -1, SDL_RENDERER_ACCELERATED);
    if (mainRenderer == NULL)
    {
        cout << "main Renderer couldn't be created";
    }
    new SDL_Event();


    loadGameObjects();
    loadGraphics();
    //^^^crutches
    bgm = Mix_LoadMUS("RESOURSES/bgm.mp3");
    windowSound = Mix_LoadWAV("RESOURSES/note.ogg");
    if(windowSound == NULL)
    {
        cout << "Could'nt load sound";
    }
    Mix_PlayMusic(bgm, 1);

}

// IT WAS LONG SUMMER
// i wish it was longer...




//Event processing
void game::handleEvents()
{
    SDL_PollEvent(mainEvent);

    event_buffer.saveEvent(mainEvent);
    keystates = SDL_GetKeyboardState(NULL);
}


//rendering function
void game::render()
{

    SDL_RenderPresent(mainRenderer);
}




















//################################UPDATE FUNCTION############################################
void game::update()
{

    SDL_RenderClear(mainRenderer);
    //processing events
    processKeyboardEvents();
    switch (event_buffer.popEvent()->type)
    {
    case SDL_QUIT:
        isRunning = false;
        break;
    }

    pictureStorage["BACKGROUND"]->processObjects(*this);
    scrollingPictureStorage["CLOUDS"]->processObjects(*this);
    npcStorage["SABER"]->processObjects(*this);
    pickableStorage["CURRY"]->processObjects(*this);
    gamePlayer->processObjects(*this);
    pictureStorage["LIGHTS"]->processObjects(*this);
    mainTextbox.loadTextBox(*this);
}
//keyboard input handling function
void game::processKeyboardEvents()
{
    if(keystates[SDL_SCANCODE_W] || keystates[SDL_SCANCODE_A] || keystates[SDL_SCANCODE_S] || keystates[SDL_SCANCODE_D] )
    {
        if(keystates[SDL_SCANCODE_W])
        {
        gamePlayer->setSpriteDirection(SPRITE_DIRECTION_UP);
        }
        if(keystates[SDL_SCANCODE_A])
        {
        gamePlayer->setSpriteDirection(SPRITE_DIRECTION_LEFT);
        }
        if(keystates[SDL_SCANCODE_S])
        {
        gamePlayer->setSpriteDirection(SPRITE_DIRECTION_DOWN);
        }
        if(keystates[SDL_SCANCODE_D])
        {
        gamePlayer->setSpriteDirection(SPRITE_DIRECTION_RIGHT);
        }
        gamePlayer->setSpriteStatus(SPRITE_STATE_MOVING);
        gamePlayer->setSpriteStatus(SPRITE_STATE_ANIMATING);
    }
    else
    {
        gamePlayer->removeSpriteStatus(SPRITE_STATE_ANIMATING);
        gamePlayer->removeSpriteStatus(SPRITE_STATE_MOVING);

    }
    if(keystates[SDL_SCANCODE_SPACE])
    {
        if(mainTextbox.getState() && TEXTBOX_STATE_SHOWN)
        {
            mainTextbox.removeState(TEXTBOX_STATE_SHOWN);
        }
    }
    if(mainEvent->type == SDL_KEYDOWN)
    {
    if(mainEvent->key.keysym.sym == SDLK_e)
    {
        gamePlayer->setState(PLAYER_STATE_USING);
        keyTimer = SDL_GetTicks();
    }
    if(keystates[SDL_SCANCODE_E] == false)
    {
        if(((SDL_GetTicks() - keyTimer) > 4))
        {
        gamePlayer->removeState(PLAYER_STATE_USING);
        }
    }
    }

    }










//#####################EVENT BUFFER###################
game::Event_Buffer::Event_Buffer()
{

}

game::Event_Buffer::~Event_Buffer()
{

}

void game::Event_Buffer::saveEvent(SDL_Event* iEvent)
{
    mEventStorage.push_back(iEvent);
}

void game::Event_Buffer::clearBuffer()
{
    mEventStorage.clear();
}


SDL_Event* game::Event_Buffer::popEvent()
{
    SDL_Event* buffer;
    buffer = mEventStorage.front();
    mEventStorage.pop_back();
    return buffer;

}



//-----------SUPPLEMENTARY FUNCTIONS

bool game::collisionCheck(SDL_Rect a, SDL_Rect b)
{
    //The sides of the rectangles
        int leftA, leftB;
        int rightA, rightB;
        int topA, topB;
        int bottomA, bottomB;

        //Calculate the sides of rect A
        leftA = a.x;
        rightA = a.x + a.w;
        topA = a.y;
        bottomA = a.y + a.h;

        //Calculate the sides of rect B
        leftB = b.x;
        rightB = b.x + b.w;
        topB = b.y;
        bottomB = b.y + b.h;
        if( bottomA <= topB )
           {
               return false;
           }

           if( topA >= bottomB )
           {
               return false;
           }

           if( rightA <= leftB )
           {
               return false;
           }

           if( leftA >= rightB )
           {
               return false;
           }
           return true;
}

game::~game()
{


}

//###################################GAME EVENTS SECTION##################################################



//#################################LOADING FUNCTIONS SECTION##################################

void game::loadGameObjects()
{
    gamePlayer = new Player("RESOURSES/Spritesheet.png", 525, 7, *this, {45, 500, 32, 54});

    pictureStorage["BACKGROUND"] = new Picture("RESOURSES/grass.jpg",* this, {0,0,1366, 768});
    scrollingPictureStorage["CLOUDS"] = new Scrolling_Picture("RESOURSES/clouds.png", *this, {0,-300,1366, 768},
                                                                                                                                  SCROLLING_PICTURE_DIRECTION_RIGHT);
    pictureStorage["LIGHTS"] = new Picture("RESOURSES/light.png",* this, {0,0,1366, 768});
    pickableStorage["CURRY"] = new Pickable({1200, 500, 32, 32}, "RESOURSES/Curry.png", *this, "Jeanne says:", "RESOURSES/Jeanne.png", "Oh, so that's curry she talked about.");
    npcStorage["SABER"] = new Npc("RESOURSES/Spritesheet.png", 395, 7, *this, {120, 500, 32, 54});
    npcStorage["SABER"]->setDialogueLine(1, "RESOURSES/Saber.png", "Mordred says:", "I think i'm in the mood for some curry!");
    npcStorage["SABER"]->setDialogueLine(2, "RESOURSES/Jeanne.png", "Jeanne says:", "Why..what? I don't even. Huuuh. Whatever. I'll see what i can do.");
    npcStorage["SABER"]->setDialogueLine(3, "RESOURSES/Saber.png", "Mordred says:", "Would've been great! Curry is tasty, you know!");
    npcStorage["SABER"]->setDialogueLine(4, "RESOURSES/Jeanne.png", "Jeanne says:", "Yeah, yeah, i've got it.");
    npcStorage["SABER"]->setConditionText("RESOURSES/Saber.png", "Mordred says:", "Mmm.. Yummy!");
   //Delete everything below later

}

void game::loadGraphics()
{
    mainTextbox.setBackground(IMG_LoadTexture(mainRenderer, "RESOURSES/windowBackground.png"));
    mainTextbox.setWindowRect({300, 500, 800, 200});
    mainTextbox.setPortraitRect({308, 512, 143, 174});
    mainTextbox.setTextRect({486, 569, 325, 16});
    mainTextbox.setTitleRect({486, 523, 79, 16});
    mainTextbox.setMainFont("RESOURSES/FranklinGothicBook.ttf");
    mainTextbox.setTitleFont("RESOURSES/FranklinGothicBook.ttf");
    mainTextbox.setTextColor({45, 45, 45});
    mainTextbox.setTitleColor({45, 45, 45});
}





//#################################GAME_OBJECTS SECTION########################################







void game::Game_Object::processObjects(game mgame)
{

}



game::Game_Object::Game_Object()
{

}

game::Game_Object::Game_Object(SDL_Rect iRect)
    :mRect(iRect)
{

}

game::Game_Object::~Game_Object()
{

}


game::Text_rect::Text_rect()
{

}

game::Text_rect::Text_rect(SDL_Rect iRect, char *iPortraitPath, char *iTitle, char *iText)
    :mPortraitPath(iPortraitPath), mTitle(iTitle), mText(iText)
{
      mRect = iRect;
}



game::Text_rect::~Text_rect()
{

}

void game::Text_rect::processObjects(game &mgame)
{

    if(mgame.getPlayer()->getState() && PLAYER_STATE_USING)
    {
    if(mgame.collisionCheck(mgame.getPlayer()->getRect(), mRect))
    {
        mgame.mainTextbox.setPortrait(mPortraitPath, mgame);
        mgame.mainTextbox.setTitle(mTitle, mgame);
        mgame.mainTextbox.setText(mText, mgame);
        mgame.mainTextbox.setState(TEXTBOX_STATE_SHOWN);
    }
    }
}


game::Player::Player()
{

}


game::Player::Player(char *iPath, int StartX, int StartY, game iGame, SDL_Rect iRect)
    :mMovementTimer(SDL_GetTicks()), mState(NULL), mInventory(NULL)
{
    mRect = iRect;
    mSprite.loadSpriteFromSpriteSheet(iPath,  iGame, StartX, StartY);
    mSprite.setUpdateTimer(SDL_GetTicks());
}

void game::Player::processObjects(game mgame)
{
    cout << mInventory << endl;
    if(mSprite.getCurrentSpriteStatus() && SPRITE_STATE_ANIMATING)
    {
       mSprite.updateFrame();
    }
       mSprite.loadFrame(mSprite.getCurrentFrame(), mgame, mRect);
       if( mSprite.getCurrentSpriteStatus() && SPRITE_STATE_MOVING)
       {
       if (SDL_GetTicks() - mMovementTimer>= 20)
       {

           switch(mSprite.getCurrentDirection())
           {

           case SPRITE_DIRECTION_UP:
                                                              if(mRect.y > 430)
                                                              {
                                                              mRect.y += -2;
                                                              }
                                                               break;
           case SPRITE_DIRECTION_LEFT:
                                                               mRect.x +=-2;
                                                               break;
           case SPRITE_DIRECTION_RIGHT:
                                                               mRect.x +=2;
                                                               break;
           case SPRITE_DIRECTION_DOWN:
                                                               mRect.y += 2;
                                                               break;
           }
           mMovementTimer = SDL_GetTicks();
       }
       }
}


void game::Npc::processObjects(game& mgame)
{

    if(mSprite.getCurrentSpriteStatus() && SPRITE_STATE_ANIMATING)
    {
       mSprite.updateFrame();
    }
    mSprite.loadFrame(mSprite.getCurrentFrame(), mgame, mRect);

    if((mgame.getPlayer()->getState() && PLAYER_STATE_USING))
    {
        if(mgame.collisionCheck(mgame.getPlayer()->getRect(), mRect))
    {
            if((mgame.getPlayer()->getInventory() == ITEM_LIST_CURRY) == false)
            {
            mDialogue[mDialogueIterator]->processObjects(mgame);
            if(mDialogueIterator <  mDialogue.size())
            {
            mDialogueIterator += 1;
            Mix_PlayChannel( -1, mgame.windowSound, 0 );
            }
            else
            {
                mDialogueIterator = 1;
                Mix_PlayChannel( -1, mgame.windowSound, 0 );
            }
            }
            else
            {
                mConditionText->processObjects(mgame);
            }
    }
        mgame.getPlayer()->removeState(PLAYER_STATE_USING);
      }


}


game::Npc::Npc()
{

}

game::Npc::Npc(char *iPath, int StartX, int StartY, game iGame, SDL_Rect iRect)
    :mMovementTimer(SDL_GetTicks()), mState(NULL), mDialogueIterator(1),
      mDialogueTimer(SDL_GetTicks())
{
    mRect = iRect;
    mSprite.loadSpriteFromSpriteSheet(iPath,  iGame, StartX, StartY);
    mSprite.setUpdateTimer(SDL_GetTicks());
    mSprite.setCurrentSpriteStatus(SPRITE_STATE_ANIMATING);
    mSprite.removeCurrentSpriteStatus(SPRITE_STATE_MOVING);
}


game::Npc::~Npc()
{

}


game::Pickable::Pickable()
{

}
game::Pickable::Pickable(SDL_Rect iRect, char *iPicturePath, game& mgame, char *iTitle, char *iPortraitPath, char *iText)
{
       mState = PICKABLE_LYING;
       SDL_Surface* buffer;
       mRect = iRect;
       buffer = IMG_Load(iPicturePath);
       mTexture = SDL_CreateTextureFromSurface(mgame.getRenderer(), buffer);
       mPickText = new Text_rect(mRect, iPortraitPath, iTitle, iText);

}

game::Pickable::~Pickable()
{

}

void game::Pickable::processObjects(game& mgame)
{

        if(mgame.collisionCheck(mgame.getPlayer()->getRect(), mRect))
        {
            mgame.getPlayer()->putItem(ITEM_LIST_CURRY);
            mState = 0;
            mPickText->processObjects(mgame);
        }


    if(mState && PICKABLE_LYING)
    {
        SDL_RenderCopy(mgame.getRenderer(), mTexture, NULL, &mRect);
    }
    else
    {

    }

}


game::Picture::Picture()
{

}


game::Picture::Picture(char *iPath, game iGame, SDL_Rect iTargetRect)
{
    mRect = iTargetRect;
    SDL_Surface* buffer = IMG_Load(iPath);
    mTexture = SDL_CreateTextureFromSurface(iGame.getRenderer(), buffer);
    delete buffer;
}

game::Picture::~Picture()
{

}

void game::Picture::processObjects(game mgame)
{
    SDL_RenderCopy(mgame.getRenderer(), mTexture, NULL, &mRect);
}


void game::Scrolling_Picture::processObjects(game mgame)
{
    if(mRect.x > 1366)
    {
        mRect.x = -1366;
    }
    if (mRect.y > 768)
    {
        mRect.y = -768;
    }

    if(SDL_GetTicks() - mTimer > 60)
    {
        switch(mScrollingDirection)
        {
                 case SCROLLING_PICTURE_DIRECTION_DOWN:
                 mRect.y += 1;
                 break;
                 case SCROLLING_PICTURE_DIRECTION_LEFT:
                 mRect.x += -1;
                 break;
                 case SCROLLING_PICTURE_DIRECTION_RIGHT:
                  mRect.x += +1;
                  break;
                 case SCROLLING_PICTURE_DIRECTION_UP:
                 mRect.y += -1;
                  break;
        }
        mTimer = SDL_GetTicks();
    }
    SDL_RenderCopy(mgame.getRenderer(), mTexture, NULL, &mRect);
}




game::Scrolling_Picture::Scrolling_Picture()
{

}

game::Scrolling_Picture::Scrolling_Picture(char *iPath, game iGame, SDL_Rect iTargetRect, unsigned int iDirection)
{
    mRect = iTargetRect;
    mScrollingDirection = iDirection;
    SDL_Surface* buffer = IMG_Load(iPath);
    mTexture = SDL_CreateTextureFromSurface(iGame.getRenderer(), buffer);
    delete buffer;
}

game::Scrolling_Picture::~Scrolling_Picture()
{

}




//########################################## GRAPHICS SECTION #############################################


//Sprites and animation

game::Sprite::Sprite()
    :mUpdateTimer(SDL_GetTicks()), mCurrentFrame(1), mSpriteState(0), mSpriteDirection(1)
{
}

game::Sprite::~Sprite()
{

}

void game::Sprite::loadSpriteFromSpriteSheet(char* iPath, game mgame, int startingX, int startingY)
{
    //where startingX and startingY are the beginning positions of current spritesheet
    int x, y;
    mSpriteSurface = IMG_Load(iPath);
    if (mSpriteSurface == NULL)
    {
        cout << "image at " << iPath << " could not be loaded";
    }
    else
    {
        SDL_SetColorKey(mSpriteSurface, SDL_TRUE,
            SDL_MapRGB(mSpriteSurface->format,
            43, 133, 133));
        mSpriteTexture = SDL_CreateTextureFromSurface(mgame.getRenderer(),
            mSpriteSurface);
        if (mSpriteTexture == NULL)
        {
            cout << "conversion into texture failed at path " << iPath;
        }
        else
        {
            x = startingX;
            y = startingY;
            for (int i = 1; i <= 16; i++)
            {

                //where (23, 40) are sprite sizes
                mSpriteSheet[i] = { x, y, 23, 40 };
                x = x + 32;
                if (i % 4 == 0 && i > 0)
                {
                    x = startingX;
                    y = y + 47;
                }
            }
        }

    }

}


void game::Sprite::loadFrame(int iFrame, game mgame, SDL_Rect targetRect)
{

         SDL_RenderCopy(mgame.getRenderer(), mSpriteTexture, &mSpriteSheet[iFrame], &targetRect);
}


void game::Sprite::updateFrame()
{
    if (mSpriteDirection && SPRITE_STATE_ANIMATING)
    {

        if ((SDL_GetTicks() - mUpdateTimer) >= 200)
        {
            mCurrentFrame = mCurrentFrame + 1;
            mUpdateTimer = SDL_GetTicks();
        }
        if(mCurrentFrame > (mSpriteDirection+3) || mCurrentFrame <(mSpriteDirection))
        {
            mCurrentFrame = mSpriteDirection;
        }

    }
}

//Textboxes and menus

game::Text_box::Text_box()
    :mPortrait(NULL), mBackground(NULL), mState(NULL)
{

}

game::Text_box::~Text_box()
{

}




void game::Text_box::setText(char* iText, game mgame)
{
           SDL_Surface* buffer;
           buffer = TTF_RenderText_Blended_Wrapped(mTextFont, iText, mTextColor, 528);
           mTextRect.h = buffer->h;
           mTextRect.w = buffer->w;
           mTextTexture = SDL_CreateTextureFromSurface(mgame.getRenderer(), buffer);
           delete buffer;


}

void game::Text_box::setTitle(char *iTitle, game mgame)
{
          SDL_Surface* buffer;
          buffer = TTF_RenderText_Blended(mTitleFont, iTitle, mTitleColor);
          mTitleTexture = SDL_CreateTextureFromSurface(mgame.getRenderer(), buffer);
          delete buffer;
}

void game::Text_box::loadTextBox(game mgame)
{

    if(mState && TEXTBOX_STATE_SHOWN)
    {
    SDL_RenderCopy(mgame.getRenderer(), mBackground, NULL, &mWindowRect);
    SDL_RenderCopy(mgame.getRenderer(), mPortrait, NULL, &mPortraitRect);
    SDL_RenderCopy(mgame.getRenderer(), mTextTexture, NULL, &mTextRect);
    SDL_RenderCopy(mgame.getRenderer(), mTitleTexture, NULL, &mTitleRect);
    }


}
